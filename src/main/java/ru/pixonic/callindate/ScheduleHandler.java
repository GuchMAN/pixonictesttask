package ru.pixonic.callindate;

import java.io.Closeable;
import java.time.Instant;
import java.util.concurrent.*;

public class ScheduleHandler implements Closeable{

    final private ScheduledExecutorService scheduledExecutorService;

    public ScheduleHandler() {
        scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
    }

    public <T> Future<T> onReceive(Instant instant, Callable<T> callable) {
        long delay = instant.toEpochMilli() - Instant.now().toEpochMilli();
        delay = delay > 0 ? delay : 0;
        return scheduledExecutorService.schedule(callable::call, delay, TimeUnit.MILLISECONDS);
    }

    @Override
    public void close() {
        scheduledExecutorService.shutdown();
        try {
            boolean terminatedSuccessfully = scheduledExecutorService.awaitTermination(1, TimeUnit.SECONDS);
            if (terminatedSuccessfully){
                return;
            }
            throw new IllegalStateException("Couldn't terminate scheduledService");
        } catch (InterruptedException e) {
            //TODO log
        }

    }
}
