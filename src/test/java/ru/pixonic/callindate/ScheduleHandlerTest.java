package ru.pixonic.callindate;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ScheduleHandlerTest {

    private ScheduleHandler handler;

    @Before
    public final void init() {
        handler = new ScheduleHandler();
    }


    @Test
    public void callableProcessed() throws Exception {
        Future<Boolean> result = handler.onReceive(Instant.now(), () -> true);

        assertTrue(result.get());
    }

    @Test
    public void sameTimeCallableProcessedInFifoOrder() throws Exception {
        String expectedFirst = "first";
        String expectedSecond = "second";
        Deque<String> orderNames = new ConcurrentLinkedDeque<>(Arrays.asList(expectedFirst, expectedSecond));

        Instant now = Instant.now();
        Future<String> resultFirst = handler.onReceive(now, orderNames::pollFirst);
        Future<String> resultSecond = handler.onReceive(now, orderNames::pollFirst);

        assertEquals(expectedFirst, resultFirst.get());
        assertEquals(expectedSecond, resultSecond.get());
    }

    @Test
    public void callJustInTime() throws Exception {
        long timeBefore = System.currentTimeMillis();
        final long expectedDelay = 500;
        Instant callTime = Instant.now().plus(expectedDelay, ChronoUnit.MILLIS);
        handler.onReceive(
                callTime,
                () -> true
        ).get();
        long realDelay = System.currentTimeMillis() - timeBefore;
        assertTrue(realDelay >= expectedDelay);
    }

    @Test(expected = TimeoutException.class)
    public void earliestProcessedSeparatelyFromLatest() throws Exception {
        String expectedFirst = "first";
        String expectedSecond = "second";
        Deque<String> orderNames = new ConcurrentLinkedDeque<>(Arrays.asList(expectedFirst, expectedSecond));

        Instant now = Instant.now();
        Future<String> resultFirst = handler.onReceive(now, orderNames::pollFirst);
        Future<String> resultSecond = handler.onReceive(now.plus(1, ChronoUnit.DAYS), orderNames::pollFirst);

        assertEquals(expectedFirst, resultFirst.get());
        resultSecond.get(1, TimeUnit.SECONDS);
    }


    @After
    public final void close() {
        try {
            handler.close();
        } catch (Exception ignore) {
        }
    }
}
